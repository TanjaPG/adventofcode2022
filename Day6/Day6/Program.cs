﻿namespace Day6
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var input = File.ReadAllText(@"input.txt");
            var list = new List<KeyValuePair<int, string>>();

            Console.Write("Enter integer: ");
            var part = int.Parse(Console.ReadLine()!);

            var total = part == 1 ? 4 : 14;

            var count = total - 1;

            for (int i = 0; i < input.Length; i++)
            {
                if (!input.Substring((i+1), count).Contains(input[i].ToString()))
                {
                    list.Add(new KeyValuePair<int, string>(i, input[i].ToString()));
                    count--;

                    if (list.Count == total)
                        break;                   
                }
                else if(list.Count > 0)
                {
                    list.Clear();
                    count = total - 1;
                }
            }
            Console.WriteLine(int.Parse(list.Select(x => x.Key.ToString()).Last())+1);
        }
    }
}