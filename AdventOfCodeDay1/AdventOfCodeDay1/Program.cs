﻿// See https://aka.ms/new-console-template for more information

using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;

internal class Program
{
    private static void Main(string[] args)
    {
        //First solution was not the best performance by me (heh) but decided to continue with it and then redo since I already was into it.
        //So the section below is how I first read the file and then handled the list in methods.

        /*var input = File.ReadAllLines(@"input.txt");

        foreach (string line in input)
        {
            if (!string.IsNullOrEmpty(line))
            {
                measurementList.Add(int.Parse(line));
            }
            else
            {
                measurementList.Add(-1);

           }
        }*/

        var input = File.ReadAllLines(@"input.txt");
        var measurementList = new List<int>();
        var elfList = new List<Elf>();
        int number;

        foreach (string line in input)
        {
            if (!string.IsNullOrEmpty(line))
            {
                measurementList.Add(int.Parse(line));
            }
            else
            {
                List<int> oneElfList = new List<int>(measurementList);
                var elf = new Elf(oneElfList);
                elfList.Add(elf);
                measurementList.Clear();
            }
        }

        string choice;
        Console.Write("Enter integer: ");
        choice = Console.ReadLine()!;
        var part = int.Parse(choice);

        if (part == 1)
        {
            number = Part1(measurementList);
            Console.WriteLine(number.ToString());
        }
        else if (part == 2)
        {
            var total = Part2(measurementList);
            Console.WriteLine(total.ToString());

        }
        //enhanced part 1
        else if (part == 3)
        {
            var topElf = elfList.Max(x => x.Calories.Sum());
            Console.WriteLine(topElf.ToString());
        }
        //enhanced part 2
        else if (part == 4)
        {
            var topThreeElfs = elfList.Select(x => x.Calories.Sum()).OrderByDescending(x => x).Take(3).Sum();
            Console.WriteLine(topThreeElfs.ToString());
        }
    }
    static int Part1(List<int> measurementList)
    {
        var heigestNumber = 0;
        var position = 0;

        //Below is also a bit why??
        //If this was the solution to go for I should have done it the other way around.
        //If it's NOT -1 just add numbers to an int and then compare two varibles. 
        //Instead I complicated it with skip and take... (: 

        for (int i = 0; i < measurementList.Count; i++)
        {
            var sum = 0;

            if (measurementList[i] == -1)
            {
                sum = measurementList.Skip(position).Take(i - position).Sum();
                if (sum > heigestNumber)
                {
                    heigestNumber = sum;
                }
                position = i + 1;
            }

            if (i == measurementList.Count - 1)
            {
                sum = measurementList.Skip(position).Take(i - position + 1).Sum();

                if (sum > heigestNumber)
                {
                    heigestNumber = sum;
                }
            }
        }
        return heigestNumber;
    }
    static int Part2(List<int> measurementList)
    {
        var position = 0;
        var sumCaloriesOnElf = new List<int>();

        for (int i = 0; i < measurementList.Count; i++)
        {
            var sum = 0;

            if (measurementList[i] == -1)
            {
                sum = measurementList.Skip(position).Take(i - position).Sum();
                sumCaloriesOnElf.Add(sum);
                position = i + 1;
            }

            if (i == measurementList.Count - 1)
            {
                sum = measurementList.Skip(position).Take(i - position + 1).Sum();
                sumCaloriesOnElf.Add(sum);
            }
        }
        var total = sumCaloriesOnElf.OrderByDescending(x => x).ToList().Take(3).Sum();
        return total;
    }
}
internal record Elf(List<int> Calories);


