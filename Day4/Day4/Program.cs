﻿using System.Reflection;
using System.Runtime.CompilerServices;

namespace Day4
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var input = File.ReadAllLines(@"input.txt");
            var pairList = new List<Pair>();

            foreach (string line in input)
            {
                string[] parts = line.Split(',');
                var assignmentList = new List<Assignment>();

                foreach (var assignment in parts)
                {
                    var number = assignment.Split('-');
                    Assignment elf = new Assignment(Int32.Parse(number[0]), Int32.Parse(number[1]));
                    assignmentList.Add(elf);
                }

                var pair = new Pair(assignmentList.First(), assignmentList.Last());
                pairList.Add(pair);
            }

            var part1 = FullyContains(pairList);
            var part2 = PartlyContain(pairList);
        }

        internal static int FullyContains(List<Pair> pairs)
        {
            int count = 0;
            foreach (var pair in pairs)
            {
                var isFully = ((pair.first.Max <= pair.second.Max && pair.first.Min >= pair.second.Min) ||
                    (pair.second.Max <= pair.first.Max && pair.second.Min >= pair.first.Min));
                if (isFully)
                    count++;
            }

            return count;
        }

        internal static int PartlyContain(List<Pair> pairs)
        {
            int count = 0;
            foreach (var pair in pairs)
            {
                var firstPart = Enumerable.Range(pair.first.Min, (pair.first.Max - pair.first.Min + 1));
                var secondPart = Enumerable.Range(pair.second.Min, (pair.second.Max - pair.second.Min + 1));
                var contain = firstPart.Any(x => secondPart.Any(y => y == x));

                if (contain)
                    count++;
            }

            return count;
        }
    }

    internal record Assignment(int Min, int Max);
    internal record Pair(Assignment first, Assignment second);
}