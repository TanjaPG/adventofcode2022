﻿using System.IO;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Text;

namespace Day3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var letter = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

            var input = File.ReadAllLines(@"input.txt");
            var rucksackList = new List<Rucksack>();
            var rucksackGroupList = new List<RucksackGroup>();
            var rucksackLine = new List<string>();

            string choice;
            Console.Write("Enter integer: ");
            choice = Console.ReadLine()!;
            var part = int.Parse(choice);

            if (part == 1)
            {
                foreach (string line in input)
                {
                    var length = line.Length;

                    var compartmentA = line[0..(length / 2)];
                    var compartmentB = line[(length / 2)..];
                    Rucksack rucksack = new Rucksack(compartmentA, compartmentB);
                    rucksackList.Add(rucksack);
                }

                var part1 = compareCompartments(rucksackList, letter);
                Console.WriteLine(part1.ToString());
            }
            else
            {
                foreach (string line in input)
                {
                    rucksackLine.Add(line);
                }

                for (int i = 0; i < rucksackLine.Count; i +=3)
                {
                    var rucksackGroup = rucksackLine.Skip(i).Take(3).ToList();
                    RucksackGroup group = new RucksackGroup(rucksackGroup);
                    rucksackGroupList.Add(group);
                }

                var part2 = compareGroup(rucksackGroupList, letter);
                Console.WriteLine(part2.ToString());
            }
        }
        static int compareCompartments(List<Rucksack> rucksacks, string letter)
        {
            var points = 0;
            foreach(var rucksack in rucksacks)
            {
                for (int i = 0; i < rucksack.compartmentA.Length; i++)
                {
                    if (rucksack.compartmentB.Contains(rucksack.compartmentA[i]))
                    {
                        points += letter.IndexOf(rucksack.compartmentA[i]) + 1;
                        break;
                    }
                }
            }

            return points;           
        }
        static int compareGroup(List<RucksackGroup> groups, string letters)
        {
            var point = 0;
            foreach (var group in groups)
            {
                foreach (var item in group.rucksacks[0])
                {
                    if (group.rucksacks[1].Contains(item) && group.rucksacks[2].Contains(item))
                    {
                        point += letters.IndexOf(item) + 1;
                        break;
                    }
                }
            }
            return point;
        }
    }
    internal record Rucksack(string compartmentA, string compartmentB);
    internal record RucksackGroup(List<string> rucksacks);
}

