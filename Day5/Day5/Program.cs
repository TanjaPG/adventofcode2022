﻿using System;
using System.Data.SqlTypes;
using System.Diagnostics.Metrics;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace Day5
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var input = File.ReadAllLines(@"input.txt");
            List<string> output = new List<string>();
            List<Move> moves = new List<Move>();
            List<string> stackOrderList = new List<string>();
            List<List<string>> stackList = new List<List<string>>();
            int start = 0;
            string choice;
            Console.Write("Enter integer: ");
            choice = Console.ReadLine()!;
            var part = int.Parse(choice);


            foreach (string line in input)
            {
                if (line.StartsWith("m"))
                {
                    var instruction = line.Split(' ');
                    var amount = int.Parse(instruction[1]);
                    var from = int.Parse(instruction[3]);
                    var to = int.Parse(instruction[5]);

                    Move moveInstruction = new Move(amount, from, to);
                    moves.Add(moveInstruction);
                }
                else if (line != "")
                {
                    output.Add(line);
                }
            }

            int amountOfStacks = int.Parse(output.Last().Split(' ').Where(x => !string.IsNullOrWhiteSpace(x)).Max()!);
            output.RemoveAt(amountOfStacks - 1);

            for (int i = 0; i < amountOfStacks; i++)
            {
                int count = 3;
                stackOrderList.Clear();

                output.ForEach(x => stackOrderList.Add(x.Substring(start, count)));
                stackList.Add(new List<string>(stackOrderList.Where(x => !string.IsNullOrWhiteSpace(x))));

                start += 4;
            }
            if(part == 1)
                moveStoragePart1(stackList, moves);
            else
                moveStoragePart2(stackList, moves);

        }
        internal static void moveStoragePart1(List<List<string>> stackList, List<Move> moves)
        {
            foreach (var move in moves)
            {
                for (int i = 0; i < move.AmountToMove; i++)
                {
                    stackList[move.ToStorage - 1].Insert(0, stackList[move.FromStorage - 1][0]);
                    stackList[move.FromStorage - 1].RemoveAt(0);

                }
            }

            stackList.ForEach(x => Console.WriteLine(x[0].ToString().Replace("[", string.Empty).Replace("]", string.Empty)));

        }
        internal static void moveStoragePart2(List<List<string>> stackList, List<Move> moves)
        {
            foreach (var move in moves)
            {
                var moveBulkIndex = move.AmountToMove -1;

                for (int i = 0; i < move.AmountToMove; i++)
                {
                    stackList[move.ToStorage - 1].Insert(0, stackList[move.FromStorage - 1][moveBulkIndex]);
                    moveBulkIndex--;
                }
                stackList[move.FromStorage - 1].RemoveRange(0, move.AmountToMove);
            }

            stackList.ForEach(x => Console.WriteLine(x[0].ToString().Replace("[", string.Empty).Replace("]", string.Empty)));

        }
    }
}
internal record Move(int AmountToMove, int FromStorage, int ToStorage);