﻿
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;

internal class Program
{
    private static void Main(string[] args)
    {
        var possibleVariations = AddPossibleVariations();
        var input = File.ReadAllLines(@"input.txt");
        var roundList = new List<Round>();

        foreach (string line in input)
        {
            var p1 = line[0];
            var p2 = line[2];
            var round = new Round(p1, p2);
            roundList.Add(round);
        }

        var part1 = PointsInGamePart1(roundList, possibleVariations);
        var part2 = PointsInGamePart2(roundList, possibleVariations);

        Console.WriteLine(part1.ToString() + "," + part2.ToString());
    }


    static int PointsInGamePart1(List<Round> rounds, List<PossibleVariations> variations)
    {
        var points = 0;

        foreach (var round in rounds)
        {
            points += variations.Where(x => x.Round == round).Select(x => x.PointsP2).FirstOrDefault();
        }

        return points;
    }

    static int PointsInGamePart2(List<Round> rounds, List<PossibleVariations> variations)
    {
        var points = 0;
        foreach (var round in rounds)
        {
            if (round.SecondPlayer == 'Y')
            {
                points += variations.Where(x => (x.Round.FirstPlayer == round.FirstPlayer) && (x.Winner == 'X')).Select(x => x.PointsP2).FirstOrDefault();
            }
            else if (round.SecondPlayer == 'X')
            {
                points += variations.Where(x => (x.Round.FirstPlayer == round.FirstPlayer) && (x.Winner == '1')).Select(x => x.PointsP2).FirstOrDefault();
            }
            else
            {
                points += variations.Where(x => (x.Round.FirstPlayer == round.FirstPlayer) && (x.Winner == '2')).Select(x => x.PointsP2).FirstOrDefault();
            }
        }

        return points;
    }
    static List<PossibleVariations> AddPossibleVariations()
    {
        List<PossibleVariations> possibleVariations = new List<PossibleVariations>
        {
            new PossibleVariations(new Round ('A','Y'), '2', 1, 8),
            new PossibleVariations(new Round ('A','X'), 'X', 4, 4),
            new PossibleVariations(new Round ('A','Z'), '1', 7, 3),
            new PossibleVariations(new Round ('B','Y'), 'X', 5, 5),
            new PossibleVariations(new Round ('B','X'), '1', 8, 1),
            new PossibleVariations(new Round ('B','Z'), '2', 2, 9),
            new PossibleVariations(new Round ('C','Y'), '1', 9, 2),
            new PossibleVariations(new Round ('C','X'), '2', 3, 7),
            new PossibleVariations(new Round ('C','Z'), 'X', 6, 6),
        };

        return possibleVariations;
    }
}

internal record Round(char FirstPlayer, char SecondPlayer);
internal record PossibleVariations(Round Round, char Winner, int PointsP1, int PointsP2);